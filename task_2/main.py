import psycopg2
import time

time.sleep(20)

conn = psycopg2.connect(
    dbname="root",
    user="root",
    password="root",
    host="172.19.0.2",
    port="5432"
)

cur = conn.cursor()

cur.execute("SELECT Students.first_name, Students.last_name FROM Students INNER JOIN RecordBooks ON Students.student_id = RecordBooks.student_id WHERE RecordBooks.teacher_name = 'Иванов Д.А.'")
rows = cur.fetchall()

with open('result.txt', 'w') as file:
    file.write("Имя\t\tФамилия\n")
    for row in rows:
        file.write(f"{row[0]}\t{row[1]}\n")

cur.close()
conn.close()