CREATE TABLE Students (
student_id SERIAL PRIMARY KEY,
first_name VARCHAR(50),
last_name VARCHAR(50),
birth_date DATE,
record_book_id INT
);

CREATE TABLE RecordBooks (
record_book_id SERIAL PRIMARY KEY,
student_id INT,
subject VARCHAR(50),
exam_date DATE,
teacher_name VARCHAR(100),
FOREIGN KEY (student_id) REFERENCES Students(student_id)
);

INSERT INTO Students (first_name, last_name, birth_date, record_book_id)
VALUES
('Иван', 'Иванов', '1998-05-15', 1),
('Петр', 'Петров', '1997-10-22', 2),
('Мария', 'Сидорова', '1999-03-30', 3),
('Елена', 'Козлова', '1996-12-18', 4),
('Алексей', 'Смирнов', '1997-08-07', 5),
('Ольга', 'Никитина', '1998-06-25', 6),
('Дмитрий', 'Кузнецов', '1999-09-17', 7),
('Татьяна', 'Белова', '1997-11-28', 8),
('Владимир', 'Андреев', '1998-02-14', 9),
('Светлана', 'Григорьева', '1996-07-20', 10);

INSERT INTO RecordBooks (student_id, subject, exam_date, teacher_name)
VALUES
(1, 'Математика', '2022-01-25', 'Соколов К.В.'),
(1, 'История', '2022-01-22', 'Иванов Д.А.'),
(1, 'Математика', '2022-01-19', 'Петрова О.Н.'),
(1, 'История', '2022-01-21', 'Козлова А.П.'),
(2, 'Математика', '2022-01-17', 'Сидоров Г.М.'),
(2, 'Физика', '2022-02-01', 'Иванова Н.П.'),
(2, 'Математика', '2022-01-30', 'Петров А.И.'),
(2, 'История', '2022-01-28', 'Сидоров М.В.'),
(3, 'Математика', '2022-01-24', 'Козлов И.Г.'),
(3, 'Физика', '2022-01-26', 'Смирнова Е.А.'),
(3, 'Математика', '2022-01-27', 'Кузнецов Л.П.'),
(3, 'Физика', '2022-01-29', 'Александров С.С.'),
(4, 'Математика', '2022-01-23', 'Федоров Д.Д.'),
(4, 'История', '2022-02-02', 'Соловьев А.В.'),
(4, 'Математика', '2022-02-03', 'Тарасова Е.Е.'),
(4, 'История', '2022-02-04', 'Никитина О.И.'),
(5, 'Математика', '2022-01-18', 'Королева Е.Р.'),
(5, 'История', '2022-02-05', 'Соколова Т.И.'),
(5, 'Математика', '2022-01-30', 'Лебедев В.В.'),
(5, 'История', '2022-01-31', 'Зайцев П.С.'),
(6, 'Математика', '2022-01-25', 'Соколов К.В.'),
(6, 'История', '2022-01-22', 'Иванов Д.А.'),
(6, 'Математика', '2022-01-19', 'Петрова О.Н.'),
(6, 'История', '2022-01-21', 'Козлова А.П.'),
(7, 'Математика', '2022-01-17', 'Сидоров Г.М.'),
(7, 'Физика', '2022-02-01', 'Иванова Н.П.'),
(7, 'Математика', '2022-01-30', 'Петров А.И.'),
(7, 'История', '2022-01-28', 'Сидоров М.В.'),
(8, 'Математика', '2022-01-24', 'Козлов И.Г.'),
(8, 'Физика', '2022-01-26', 'Смирнова Е.А.'),
(8, 'Математика', '2022-01-27', 'Кузнецов Л.П.'),
(8, 'Физика', '2022-01-29', 'Александров С.С.'),
(9, 'История', '2022-01-23', 'Федоров Д.Д.'),
(9, 'Математика', '2022-02-02', 'Соловьев А.В.'),
(9, 'История', '2022-02-03', 'Тарасова Е.Е.'),
(9, 'Математика', '2022-02-04', 'Никитина О.И.'),
(10, 'История', '2022-01-18', 'Королева Е.Р.'),
(10, 'Математика', '2022-02-05', 'Соколова Т.И.'),
(10, 'История', '2022-01-30', 'Лебедев В.В.'),
(10, 'Математика', '2022-01-31', 'Зайцев П.С.');
