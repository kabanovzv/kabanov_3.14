import sys

def ToUpper(input_symbol):
    if input_symbol >= 'a' and input_symbol <= 'z':
        return chr(ord(input_symbol) - ord('a') + ord('A'))
    else:
        return input_symbol


if __name__ == "__main__":
    input_symbol = sys.argv[1]
    up_symbol = ToUpper(input_symbol)
    print(f"Заглавный символ: {up_symbol}")
    print(input_symbol)

